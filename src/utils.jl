function walkpredict(dir::String; pattern::Regex = r"\.tif$", kwargs...)
    for (root, dirs, files) in walkdir(dir)
        for file in files
            if occursin(pattern, file)
                out = splitext(file) * ".csv"

                isfile(out) && continue

                @info "obtaining predictions" file=file
                predict!(out, file)
            end
        end
    end
end
