module MAXBreastCancerMitosisDetector

using Images, FileIO, LibCURL, LazyJSON

using DelimitedFiles

export predict, predict!, walkpredict

include("predict.jl")
include("utils.jl")

end # module
