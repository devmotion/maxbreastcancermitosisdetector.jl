function curl_write_cb(ptr::Ptr{UInt8}, sz::Csize_t, n::Csize_t,
                       userdata::Ptr{Cvoid})::Csize_t
    # compute total number of bytes
    nbytes = sz * n

    # save data to buffer
    buffer = unsafe_pointer_to_objref(userdata)::IOBuffer
    Base.unsafe_write(buffer, ptr, nbytes)

    # return number of processed bytes
    nbytes
end

function predict(f::Union{String,IOStream};
                 server::String = "http://127.0.0.1:5000/model/predict")
    # load image
    image = load(f)

    # initialize
    curl = curl_easy_init()
    curl == C_NULL && error("curl init failed")

    try
        # define posted form
        mime = curl_mime_init(curl)

        try
            part = curl_mime_addpart(mime)
            curl_mime_name(part, "image")
            curl_mime_filename(part, "image.png")
            curl_mime_type(part, "image/png")

            # define options
            curl_easy_setopt(curl, CURLOPT_URL, server)
            curl_easy_setopt(curl, CURLOPT_MIMEPOST, mime)
            curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1)

            let response = IOBuffer(), data = IOBuffer()
                # write response to buffer
                c_curl_write_cb = @cfunction(curl_write_cb, Csize_t,
                                             (Ptr{UInt8}, Csize_t, Csize_t, Ptr{Cvoid}))
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, c_curl_write_cb)
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, pointer_from_objref(response))

                # save predictions of non-overlapping sliding windows
                indices = (:).(64, 64, size(image))
                predictions = Array{Float64}(undef, length.(indices))
                for (i, (x, y)) in enumerate(Iterators.product(indices...))
                    # convert subregion to PNG format
                    save(Stream(format"PNG", data), view(image, (x-63):x, (y-63:y)))
                    rawdata = take!(data)

                    # add image to posted form and send it
                    curl_mime_data(part, rawdata, length(rawdata))
                    ret = curl_easy_perform(curl)

                    # check response code
                    ret == CURLE_OK || error("prediction failed")

                    # save prediction
                    json = String(take!(response))
                    @inbounds predictions[i] =
                        convert(Float64,
                                LazyJSON.value(json)["predictions"][1]["probability"])
                end

                return predictions
            end
        finally
            # clean up
            curl_mime_free(mime)
        end
    finally
        # clean up
        curl_easy_cleanup(curl)
    end
end


predict!(file::Union{String,IOStream}, img::Union{String,IOStream};
                  kwargs...) =
    writedlm(file, predict(img; kwargs...))
